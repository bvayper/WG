// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Math/UnrealMathUtility.h"


APlayerPawn::APlayerPawn()
{
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	SpringArm->SetupAttachment(RootComponent);
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);
}

//Called when game started or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmo = 5;
	PlayerControllerRef = Cast<APlayerController>(GetController());
}

//Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//If get Input - Move in direction
	if (!MovementDirection.IsZero())
	{
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementDirection.X * DeltaTime * MovementSpeed;
		NewLocation += GetActorRightVector() * MovementDirection.Y * DeltaTime * MovementSpeed;
		SetActorLocation(NewLocation, true);
	}
	//Camera Input
	{
		FRotator NewRotation = GetActorRotation();
		NewRotation.Yaw += CameraInput.X;
		SetActorRotation(NewRotation);
	}
	{
		FRotator NewRotation = SpringArm->GetComponentRotation();
		NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.Y, -80.0f, -15.0f);
		SpringArm->SetWorldRotation(NewRotation);
	}
}

void APlayerPawn::Fire()
{
	if (CurrentAmmo <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s has no ammo to fire"), *GetOwner()->GetName());
		return;
	}
	CurrentAmmo -= 1;
	UE_LOG(LogTemp, Warning, TEXT("after shot %d ammo left"), CurrentAmmo);
	Super::Fire();
}

void APlayerPawn::AltFire()
{
	if (CurrentAmmo <= 2)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s has no ammo to altFire"), *GetOwner()->GetName());
		return;
	}
	CurrentAmmo -= 3;
	UE_LOG(LogTemp, Warning, TEXT("after altFire %d ammo left"), CurrentAmmo);
	Super::AltFire();
}


void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerPawn::Fire);
	PlayerInputComponent->BindAction("AltFire", IE_Pressed, this, &APlayerPawn::AltFire);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveAside", this, &APlayerPawn::MoveAside);
	PlayerInputComponent->BindAxis("CamPitch", this, &APlayerPawn::PitchCamera);
	PlayerInputComponent->BindAxis("CamYaw", this, &APlayerPawn::YawCamera);
}

void APlayerPawn::HandleDestruction()
{
	Super::HandleDestruction();

	bIsPlayerAlive = false;
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
}

bool APlayerPawn::GetIsPlayerAlive()
{
	return bIsPlayerAlive;
}

void APlayerPawn::MoveForward(float Value)
{
	MovementDirection.X = FMath::Clamp(Value, -1.f, 1.f);
}

void APlayerPawn::MoveAside(float Value)
{
	MovementDirection.Y = FMath::Clamp(Value, -1.f, 1.f);
}

void APlayerPawn::PitchCamera(float Value)
{
	CameraInput.Y = Value;
}

void APlayerPawn::YawCamera(float Value)
{
	CameraInput.X = Value;
}