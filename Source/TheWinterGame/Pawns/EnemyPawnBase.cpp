// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"

void AEnemyPawnBase::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(FireRateTimerHandle, this, &AEnemyPawnBase::CheckFireCondition, FireRate, true);

	PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void AEnemyPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!PlayerPawn || ReturnDistanceToPlayer() > FireRange)
	{
		return;
	}
}

void AEnemyPawnBase::HandleDestruction()
{
	Super::HandleDestruction();
	Destroy();
}

void AEnemyPawnBase::CheckFireCondition()
{
	if (!PlayerPawn || !PlayerPawn->GetIsPlayerAlive())
	{
		return;
	}
	if (ReturnDistanceToPlayer() <= FireRange)
	{
		Fire();
	}
}

float AEnemyPawnBase::ReturnDistanceToPlayer()
{
	if (!PlayerPawn)
	{
		return 0.f;
	}
	return FVector::Dist(PlayerPawn->GetActorLocation(), GetActorLocation());
}

