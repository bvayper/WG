// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePawn.h"
#include "Components/CapsuleComponent.h"
#include "TheWinterGame/Actors/ProjectileBase.h"
#include "TheWinterGame/Components/HealthComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABasePawn::ABasePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
	RootComponent = CapsuleComp;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	BaseMesh->SetupAttachment(RootComponent);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(BaseMesh);
	AltProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("AltProjectile Spawn Point"));
	AltProjectileSpawnPoint->SetupAttachment(BaseMesh);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
}

void ABasePawn::Fire()
{
	if (ProjectileClass)
	{
		FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = ProjectileSpawnPoint->GetComponentRotation();

		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, SpawnLocation, SpawnRotation);
		TempProjectile->SetOwner(this);

		UE_LOG(LogTemp, Warning, TEXT("%s shoot somewhere"), *GetOwner()->GetName());
	}
}

void ABasePawn::AltFire()
{
	if (AltProjectileClass)
	{
		FVector SpawnLocation = AltProjectileSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = AltProjectileSpawnPoint->GetComponentRotation();

		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(AltProjectileClass, SpawnLocation, SpawnRotation);
		TempProjectile->SetOwner(this);

		UE_LOG(LogTemp, Warning, TEXT("%s use alternative fire"), *GetOwner()->GetName());
	}
}

void ABasePawn::HandleDestruction()
{
	//TODO some effects (particle/ sound/ camera shake)
}