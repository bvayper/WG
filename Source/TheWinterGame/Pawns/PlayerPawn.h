// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "PlayerPawn.generated.h"

class USpringArmComponent;
class UCameraComponent;
class HealthComponent;

UCLASS()
class THEWINTERGAME_API APlayerPawn : public ABasePawn
{
	GENERATED_BODY()
	
private:
	//Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* SpringArm;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;

	FVector2D CameraInput;
	FVector MovementDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float MovementSpeed = 500.f;

	APlayerController* PlayerControllerRef;
	bool bIsPlayerAlive = true;

	void MoveForward(float Value);
	void MoveAside(float Value);
	void PitchCamera(float Value);
	void YawCamera(float Value);

public:

	APlayerPawn();

	void Fire();
	void AltFire();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void HandleDestruction() override;

	bool GetIsPlayerAlive();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentAmmo = 1;

protected:

	virtual void BeginPlay() override;
};
