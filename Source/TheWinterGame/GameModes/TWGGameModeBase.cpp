// Fill out your copyright notice in the Description page of Project Settings.


#include "TWGGameModeBase.h"
#include "TheWinterGame/Pawns/PlayerPawn.h"
#include "TheWinterGame/Pawns/EnemyPawnBase.h"
#include "TheWinterGame/PlayerControllers/PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"


void ATWGGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	HandleGameStart();
}

void ATWGGameModeBase::ActorDied(AActor* DeadActor)
{
	if (DeadActor == Player)
	{
		Player->HandleDestruction();
		HandleGameOver(false);
	}
	else if (AEnemyPawnBase* DestroyedEnemy = Cast<AEnemyPawnBase>(DeadActor))
	{
		DestroyedEnemy->HandleDestruction();
	}
}

void ATWGGameModeBase::HandleGameStart()
{
	Player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
}

void ATWGGameModeBase::HandleGameOver(bool PlayerWon)
{
	GameOver(PlayerWon);
}