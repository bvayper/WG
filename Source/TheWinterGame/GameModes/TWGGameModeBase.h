// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TWGGameModeBase.generated.h"

class APlayerPawn;
class AEnemyPawnBase;
class APlayerControllerBase;

UCLASS()
class THEWINTERGAME_API ATWGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:

	APlayerPawn* Player;
	APlayerControllerBase* PlayerControllerRef;

	void HandleGameStart();
	void HandleGameOver(bool PlayerWon);

public:

	void ActorDied(AActor* DeadActor);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();
	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool PlayerWon);

};