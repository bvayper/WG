// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

class ATWGGameModeBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class THEWINTERGAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

private:

	ATWGGameModeBase* GameModeRef;

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefaultHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Health = 0.f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
};
