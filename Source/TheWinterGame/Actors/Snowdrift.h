// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snowdrift.generated.h"

class UCapsuleComponent;

UCLASS()
class THEWINTERGAME_API ASnowdrift : public AActor
{
	GENERATED_BODY()
	
private:

	//Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* CapsuleComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* SnowdriftMesh;

	//Variables
		//if time left - add some animations

public:	

	//Ammo per snowdrift
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ammo Count", meta = (AllowPrivateAccess = "true"))
	int32 ACount;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, 
						class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
						bool bFromSweep, const FHitResult& SweepResult);

// Sets default values for this actor's properties
	ASnowdrift();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
