// Fill out your copyright notice in the Description page of Project Settings.


#include "Snowdrift.h"
#include "TheWinterGame/Pawns/BasePawn.h"
#include "TheWinterGame/Pawns/PlayerPawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASnowdrift::ASnowdrift()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	RootComponent = CapsuleComp;
	SnowdriftMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Snowdrift Mesh"));
	SnowdriftMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASnowdrift::BeginPlay()
{
	Super::BeginPlay();
	ACount = 3;
	
	CapsuleComp->OnComponentBeginOverlap.AddDynamic(this, &ASnowdrift::OnOverlapBegin);
}


void ASnowdrift::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerPawn* Character = Cast<APlayerPawn>(OtherActor);
	if (Character)
	{
		Character->CurrentAmmo = Character->CurrentAmmo + ACount;
		this->Destroy();
	}
}