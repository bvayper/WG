// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_OpenFire.generated.h"

/**
 * 
 */
UCLASS()
class THEWINTERGAME_API UBTTask_OpenFire : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_OpenFire();

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);

};