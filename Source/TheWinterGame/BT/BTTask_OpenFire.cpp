// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_OpenFire.h"
#include "AIController.h"
#include "TheWinterGame/Pawns/BasePawn.h"

UBTTask_OpenFire::UBTTask_OpenFire()
{
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_OpenFire::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	
	if (OwnerComp.GetAIOwner() == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ABasePawn * Character = Cast<ABasePawn>(OwnerComp.GetAIOwner()->GetPawn());
	if (Character == nullptr)
	{
		return EBTNodeResult::Failed;
	}
	Character->Fire();

	return EBTNodeResult::Succeeded;
}
